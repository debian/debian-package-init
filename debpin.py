#!/usr/bin/env python3

from argparse import ArgumentParser
from collections import Counter
from pathlib import Path
from typing import Optional
import logging
import os
import re
import subprocess
import sys

from deb_create_watch import detect_hosting_service, write_watch_file

log = logging.getLogger("debpin")

extension_lookup = {
    "c": "c",
    "css": "css",
    "h": "c",
    "js": "javascript",
    "pl": "perl",
    "py": "python",
    "rs": "rust",
}
supported_languages = set(extension_lookup.values())
languages = " ".join(sorted(supported_languages))
ci_conf_blob = """---
include:
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/salsa-ci.yml
  - https://salsa.debian.org/salsa-ci-team/pipeline/raw/master/pipeline-jobs.yml
"""

args = None


def setup_logging(debug) -> None:
    lvl = logging.DEBUG if debug else logging.INFO
    log.setLevel(lvl)
    ch = logging.StreamHandler()
    ch.setLevel(lvl)
    formatter = logging.Formatter("%(message)s")
    ch.setFormatter(formatter)
    log.addHandler(ch)


def parse_args():
    ap = ArgumentParser()
    ap.add_argument("url", help="Repository URL")
    ap.add_argument("--pkg-name", help="Debian source package name")
    ap.add_argument("-d", "--debug", action="store_true", help="Debug mode")
    ap.add_argument("--noask", action="store_true", help="Do not ask questions")
    return ap.parse_args()


def run_cmd(*a) -> None:
    log.info("$ %s", " ".join(a))
    try:
        subprocess.check_call(list(a))
    except subprocess.CalledProcessError as e:
        # log.error("Command exited with %d", e.returncode)
        # log.error("----\n%s----", e.output.decode())
        sys.exit(1)


def create_git_repo(pkgname: str) -> None:
    os.mkdir(pkgname)
    os.chdir(pkgname)
    run_cmd("git", "init", ".")


def anyglob(glob: str) -> bool:
    return any(Path(".").glob(glob))


def guess_by_extension() -> Optional[str]:
    extensions: Counter[str] = Counter()
    for rootdir, dirs, files in os.walk("."):
        if rootdir.startswith("./.git/"):
            continue
        for fn in files:
            chunks = fn.rsplit(os.extsep, 1)
            if len(chunks) > 1:
                extensions.update(
                    [
                        chunks[-1],
                    ]
                )

    summary = (f"{i[0]}: {i[1]}" for i in extensions.most_common(5))
    log.info("Most common extensions: %s", ", ".join(summary))
    for ext, _ in extensions.most_common(5):
        if ext in extension_lookup:
            lang = extension_lookup[ext]
            log.info("Guessing %s", lang)
            return lang

    return None


def infer_language() -> Optional[str]:
    if anyglob("setup.py"):
        return "python"

    if anyglob("*.gemspec"):
        return "ruby"

    if anyglob("*.cabal"):
        return "ruby"

    if anyglob("Makefile.PL") or anyglob("Build.PL"):
        return "perl"

    if anyglob("*.nimble"):
        return "nim"

    if anyglob("CMakeLists.txt"):
        return "c"

    if anyglob("Cargo.toml"):
        return "rust"

    # last resort: some guesswork
    return guess_by_extension()


def dh_make():
    """Fallback to basic dh_make"""
    # move the debian/ dir out of the way
    Path("debian").rename("debian.old")

    # run dh_make interactively
    pname = args.pkg_name
    out = subprocess.check_output(["git", "--no-pager", "tag", "-l"])
    out = out.decode().strip()
    ver = out.rsplit("/", 1)[-1]  # upstream/1.2.3
    run_cmd("dh_make", "-p", f"{pname}_{ver}")

    Path("debian.old/watch").rename("debian/watch")
    os.rmdir("debian.old")


def debianize_c() -> None:
    dh_make()


def debianize_css() -> None:
    dh_make()


def debianize_haskell() -> None:
    run_cmd("cabal-debian")


def debianize_javascript() -> None:
    dh_make()


def debianize_nim() -> None:
    run_cmd("dh-make-nim")


def debianize_perl() -> None:
    run_cmd("dh-make-perl", "--vcs", "none")


def debianize_rust() -> None:
    dh_make()
    rules = """#!/usr/bin/make -f

include /usr/share/dpkg/default.mk
include /usr/share/rustc/architecture.mk
export CFLAGS CXXFLAGS CPPFLAGS LDFLAGS
export DEB_HOST_RUST_TYPE DEB_HOST_GNU_TYPE
export PATH := /usr/share/cargo/bin:$(PATH)
export CARGO=/usr/share/cargo/bin/cargo
export CARGO_HOME=$(CURDIR)/debian/cargo_home
export CARGO_REGISTRY=$(CURDIR)/debian/cargo_registry
export DEB_CARGO_CRATE=$(DEB_SOURCE)_$(DEB_VERSION_UPSTREAM)
export DEB_BUILD_MAINT_OPTIONS=hardening=+bindnow

%:
        dh $@ 

execute_after_dh_auto_clean:
        rm -rf debian/cargo_registry


execute_before_dh_auto_configure:
        $(CARGO) prepare-debian debian/cargo_registry --link-from-system
        rm -f Cargo.lock
"""
    Path("debian/rules").write_text(rules)


def debianize_python() -> None:
    stdeb_cmd = ["python3", "setup.py", "--command-packages=stdeb.command", "debianize"]

    if "DEBFULLNAME" in os.environ and "DEBEMAIL" in os.environ:
        stdeb_cmd.append("--maintainer")
        stdeb_cmd.append('"%s <%s>"' % (os.environ["DEBFULLNAME"], os.environ["DEBEMAIL"]))
        stdeb_cmd.append("--with-python3=True")

    run_cmd(*stdeb_cmd)


def debianize_ruby() -> None:
    run_cmd("dh-make-ruby")


def write_ci_conf() -> None:
    log.debug("Adding debian/gitlab-ci.yml")
    Path("debian/gitlab-ci.yml").write_text(ci_conf_blob)


def update_debian_control(homepage: str) -> None:
    """Uncomment Vcs lines, set homepage"""
    cf = Path("debian/control")
    control = cf.read_text()
    control = re.sub("\n#(Vcs.*)\n", r"\n\1\n", control)
    control = re.sub("\n#(Vcs.*)\n", r"\n\1\n", control)
    control = re.sub("\nHomepage: .*\n", f"\nHomepage: {homepage}\n", control)
    cf.write_text(control)


def main() -> None:
    global args
    args = parse_args()
    setup_logging(args.debug)

    proposed_project_name, watch = detect_hosting_service(args.url, pkg_name=args.pkg_name)

    if not args.pkg_name:
        args.pkg_name = proposed_project_name

    log.info("Creating Git repository")
    create_git_repo(args.pkg_name)

    log.debug("Creating debian directory")
    os.mkdir("debian")

    log.info("Generating watch file")
    write_watch_file(watch)
    if args.debug:
        log.debug("-" * 10)
        with open("debian/watch") as f:
            for line in f:
                log.debug(line.rstrip())

        log.debug("-" * 10)

    log.debug("Generating temporary changelog file")
    run_cmd(
        "/usr/bin/dch",
        "--create",
        "--package",
        args.pkg_name,
        "--empty",
        "-v",
        "0.0.0-1",
    )

    if args.debug:
        log.debug("Running uscan test")
        run_cmd("/usr/bin/uscan", "--report")

    log.info("Importing tarball using uscan")
    run_cmd("/usr/bin/gbp", "import-orig", "--uscan", "-v", "--no-interactive")

    log.debug("Deleting temporary changelog")
    os.unlink("debian/changelog")

    log.info("Inferring language")
    language = infer_language()
    if language is None:
        if args.noask:
            log.error("No language detected")
            sys.exit(1)
        print("No language detected. Please choose: " + languages)
        language = input().lower().strip()

    elif not args.noask:
        print(f"{language} language detected. " "Press enter to accept or choose: " + languages)
        lang2 = input().lower().strip()
        language = lang2 or language

    if language not in supported_languages:
        log.error("unsupported language")
        sys.exit(1)

    funcname = f"debianize_{language}"
    globals()[funcname]()  # ugly but compact

    write_ci_conf()
    update_debian_control(args.url)
    log.debug("Adding debian directory to Git")
    run_cmd("/usr/bin/git", "add", "debian")


if __name__ == "__main__":
    main()
